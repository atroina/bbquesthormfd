#!/bin/bash

cat > /tmp/script.awk <<EOF
#!/usr/bin/awk -f
BEGIN {
	min=999999999;
	max=0;
}
{
	t1=\$3;
	getline;
	dt=\$3-t1;

	samples++;
	sum+=dt;
	sump2+=(dt*dt);

	if (min>dt) min = dt;
	if (max<dt) max = dt;

	print dt;
}

END {
	u=(sum/samples);
	var  = (samples*u*u);
	var += sump2;
        var -= 2*u*sum;
	var /= (samples-1);
	sd = sqrt(var);
	se = sd / sqrt(samples);
	ci95 = 1.96 * se;
	ci99 = 2.58 * se;
	printf("#%d, min: %.3f, max: %.3f, u: %.3f, var: %.3f, sd: %.3f, se: %.3f, ci95: %.3f, ci99: %.3f\n",
		samples, min, max, u, var, sqrt(var), se, ci95, ci99);
}
EOF
chmod a+x /tmp/script.awk

grep $1 $2 | sed 's/-/ / ' | /tmp/script.awk
