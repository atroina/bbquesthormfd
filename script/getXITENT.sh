#!/bin/bash
# $1: from (XIT)
# $2: to (ENT)
# $3: output file
for i in {1..60}
do
		grep $1 $i.txt | grep xit >> $3
		grep $2 $i.txt | grep ent >> $3
done
