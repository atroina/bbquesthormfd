package it.polimi.dei.bosp;

import it.polimi.dei.bosp.dto.Result;

import java.io.IOException;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class BbqueSTHormFDActivity extends Activity {
	/** Called when the activity is first created. */
	private static final String TAG = "BbqueSTHormFDActivity";

	private static final String DEFAULT_FPS = "1";

	private final Handler handler = new Handler();

	private static ImageView imageV = null;

	// Text viewer
	protected TextView tv, fpsTextView;
	protected SeekBar fpsBar;

	// Buttons
	private static ToggleButton filterbutton;
	private static boolean filterEnabled = false;

	private static ToggleButton camerabutton;
	private static boolean cameraEnabled = false;
	private static boolean firstRun = true;

	// Bbque constants
	private static final String APP_NAME = "BbqueSTHormFD";
	// TODO: Change recipe
	private static final String APP_RECIPE = "BbqRTLibTestApp";

	/** Flag indicating whether we have called bind on the service. */
	boolean mBound;

	/** Messenger for communicating with the service. */
	Messenger mService;

	/** Camera variables: */
	private Camera camera = null;
	Camera.Size size = null;

	byte[] cameraBuffer = null;
	byte[] cameraframe = null;

	private SurfaceView preview = null;
	private SurfaceHolder previewHolder = null;
	private boolean inPreview = false;
	private boolean cameraConfigured = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//TODO: Remove tracing when not needed anymore
//		Debug.startMethodTracing();

		/** Initialize UI */
		setContentView(R.layout.main);
		imageV = (ImageView) findViewById(R.id.imageView);

		// Init text viewer
		tv = (TextView) findViewById(R.id.editText1);
		tv.setLineSpacing(0, (float) 1.0);
		// tv.setBackgroundDrawable(r.getDrawable(R.layout.layout_gradient));
		tv.setVisibility(View.VISIBLE);
		filterbutton = (ToggleButton) findViewById(R.id.toggleButtonFilter);
		camerabutton = (ToggleButton) findViewById(R.id.toggleButtonCamera);

		fpsTextView = ((TextView) findViewById(R.id.textFps));
		fpsTextView.setText("FPS: "+DEFAULT_FPS);

		fpsBar = (SeekBar) findViewById(R.id.fpsBar);
		fpsBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				seekBar.setSecondaryProgress(seekBar.getProgress());
				if (seekBar.getProgress() == 0)
					seekBar.setProgress(1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				seekBar.setSecondaryProgress(seekBar.getProgress());
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int fpsValue,
					boolean fromUser) {
				fpsTextView.setText("FPS: "+fpsValue);
				Message msg = Message.obtain(null, STHormFDService.MSG_SET_CPS,
						"" + fpsValue);
				try {
					msg.replyTo = mMessenger;
					mService.send(msg);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		});

		/** Binding to the service. This automatically starts the Service */
		bindService(new Intent(this, STHormFDService.class), mConnection,
				Context.BIND_AUTO_CREATE);

		preview = (SurfaceView) findViewById(R.id.cpPreview);
		previewHolder = preview.getHolder();
		previewHolder.addCallback(surfaceCallback);
		previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		camera = Camera.open(0);
	}

	// TODO Destroy everything
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// Unbinding from the service
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		startPreview();
	}

	@Override
	public void onPause() {
		if (inPreview) {
			camera.stopPreview();
		}
		camera.release();
		camera = null;
		inPreview = false;
		super.onPause();
//		Debug.stopMethodTracing();
	}

	/** Class for interacting with the main interface of the service. */
	private final ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			Log.d(TAG, "onServiceConnected");
			mService = new Messenger(service);
			mBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			mService = null;
			mBound = false;
		}
	};

	/**
	 * Instantiate the target - to be sent to clients - to communicate with this
	 * instance of Service
	 */
	final Messenger mMessenger = new Messenger(new MessageHandler());

	/**
	 * Handler of incoming messages from Service (typically Service responses).
	 * The same MSG_XXX are used to receive responses regarding the calls the
	 * activity made before.
	 */
	class MessageHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			//
			case STHormFDService.MSG_DECODED:
				// TODO double buffer impl
				Bundle frame = msg.getData();
				handler.post(new DisplayUI(new Result((Bitmap) frame
						.getParcelable("bitmap"), msg.arg1)));
				Log.d(TAG, "Decoded frame received.");
				break;
			case STHormFDService.MSG_SEND_BUFFER:
				// Send frame(s) upon request:
				// Check how many frames the service asks for:
				int n = msg.arg2;
				for (int i = 0; i < n; i++) {
					Message frameMsg = Message.obtain(null,
							STHormFDService.MSG_SEND_BUFFER, cameraframe);
					frameMsg.arg1 = size.height;
					frameMsg.arg2 = size.width;
					try {
						mService.send(frameMsg);
						Log.d(TAG, "Frame " + i + " sent to Service");
					} catch (RemoteException e) {
						Log.e(TAG, "Can't send buffer message to Service");
					}
				}
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	public void btnFilter(View v) {
		if (filterbutton.isChecked()) {
			filterEnabled = true;
		} else {
			filterEnabled = false;
		}
		Message cameraMsg = Message.obtain(null, STHormFDService.MSG_FILTER_ON,
				filterEnabled ? 1 : 0, 0);
		try {
			cameraMsg.replyTo = mMessenger;
			mService.send(cameraMsg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public void btnCamera(View v) {
		if (camerabutton.isChecked()) {
			cameraEnabled = true;
			startPreview();
			// For the first execution we call the Start method:
			if (firstRun) {
				try {
					Thread.sleep(600);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				/** If bound, create execution context */
				if (!mBound) {
					Log.e(TAG, "Not bound to the service!");
				} else {
					Log.d(TAG, "Calling create...");
					Message msg = Message.obtain(null,
							STHormFDService.MSG_CREATE, 0, 0);
					msg.obj = APP_NAME + "#" + APP_RECIPE;
					try {
						msg.replyTo = mMessenger;
						mService.send(msg);
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}

				/**
				 * Set default framerate for testing. TODO -> Make this editable
				 * at runtime
				 */
				Message framerateMsg = Message.obtain(null,
						STHormFDService.MSG_SET_CPS, DEFAULT_FPS);
				try {
					framerateMsg.replyTo = mMessenger;
					mService.send(framerateMsg);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				Message msg = Message.obtain(null, STHormFDService.MSG_START,
						0, 0);
				try {
					msg.replyTo = mMessenger;
					mService.send(msg);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				firstRun = false;
			} else {
				Message msg = Message.obtain(null, STHormFDService.MSG_ENABLE,
						0, 0);
				try {
					msg.replyTo = mMessenger;
					mService.send(msg);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}

		} else {
			cameraEnabled = false;
			Log.d(TAG, "Sending message to Disable.");
			Message msg = Message.obtain(null, STHormFDService.MSG_DISABLE, 0,
					0);
			try {
				msg.replyTo = mMessenger;
				mService.send(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			stopPreview();
		}
	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy called");
		Message msg = Message.obtain(null, STHormFDService.MSG_TERMINATE, 0, 0);
		try {
			msg.replyTo = mMessenger;
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	class DisplayUI implements Runnable {
		Result result;

		DisplayUI(Result result) {
			this.result = result;
		}

		@Override
		public void run() {
			Log.v(TAG, "Runnable(DISPLAY)");
			imageV.setImageBitmap(result.getBitmap());
			if (filterEnabled)
				tv.setText("SThorm's Face Detection: "+ result.getNb());
			else
				tv.setText("Face Detection Filter Disabled:");
		}
	};

	private Camera.Size getBestPreviewSize(int width, int height,
			Camera.Parameters parameters) {
		Camera.Size result = null;

		for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
			if (size.width <= width && size.height <= height) {
				if (result == null) {
					result = size;
				} else {
					int resultArea = result.width * result.height;
					int newArea = size.width * size.height;

					if (newArea > resultArea) {
						result = size;
					}
				}
			}
		}

		return (result);
	}

	private void initPreview(int width, int height) {
		if (camera != null && previewHolder.getSurface() != null) {
			try {
				camera.setPreviewDisplay(previewHolder);
			} catch (Throwable t) {
				Log.e("PreviewDemo-surfaceCallback",
						"Exception in setPreviewDisplay()", t);
				Toast.makeText(BbqueSTHormFDActivity.this, t.getMessage(),
						Toast.LENGTH_LONG).show();
			}

			if (!cameraConfigured) {
				Camera.Parameters parameters = camera.getParameters();
//				parameters.setPreviewFormat(ImageFormat.YV12);
				size = getBestPreviewSize(width, height, parameters);

				if (size != null) {
					parameters.setPreviewSize(size.width, size.height);
					// parameters.setPreviewSize(320, 240);
					camera.setParameters(parameters);
					cameraConfigured = true;
				}
			}
		}
		Size previewSize = camera.getParameters().getPreviewSize();
		Log.d(TAG, "Size of preview: " + previewSize.width + "x"
				+ previewSize.height);
		cameraBuffer = new byte[previewSize.width * previewSize.height * 3 / 2
				+ 1];
		cameraframe = new byte[previewSize.width * previewSize.height * 3 / 2
				+ 1];
	}

	Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {
		@Override
		public void onPreviewFrame(byte[] data, Camera camera) {
			// Log.d(TAG, "previewCallback -> onPreviewFrame ENTER");
			cameraframe = data;
			camera.addCallbackBuffer(data);
			// Log.d(TAG, (data == null ?
			// "previewCallback -> Camera buffer NULL"
			// : "previewCallback -> Camera buffer has some value"));
			// Send buffer to Service
			// Log.d(TAG, "previewCallback -> buffer sent");
		}
	};

	public final void startPreview() {
		Log.d(TAG, "startPreview()");
		if (cameraConfigured && camera != null) {
			try {
				camera.setPreviewDisplay(previewHolder);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.d(TAG, "startPreview() -> setPreviewDisplay DONE");
			inPreview = true;
			camera.addCallbackBuffer(cameraBuffer);
			Log.d(TAG, "startPreview() -> addCallbackBuffer DONE");
			camera.setPreviewCallbackWithBuffer(previewCallback);
			preview.setVisibility(SurfaceView.VISIBLE);
			// camera.setOneShotPreviewCallback(previewCallback);
			Log.d(TAG, "startPreview() -> setPreviewCallbackWithBuffer DONE");
			camera.startPreview();
			Log.d(TAG, "startPreview() -> startPreview DONE");
		}
	}

	public final void stopPreview() {
		camera.stopPreview();
	}

	SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			// no-op -- wait until surfaceChanged()
			Log.d(TAG, "surfaceCallback -> surfaceCreated");
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			Log.d(TAG, "surfaceCallback -> surfaceChanged");
			initPreview(320, 240);
			if (cameraEnabled)
				startPreview();
			// Remove live preview
			preview.setVisibility(SurfaceView.GONE);
			// Timer timer = new Timer();
			// timer.scheduleAtFixedRate(new MessengerTask(), 0, 500);
			// MessengerTask updater = new MessengerTask();
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			// no-op
			Log.d(TAG, "surfaceCallback -> surfaceDestroyed");
		}
	};
}
