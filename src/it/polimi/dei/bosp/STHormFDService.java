package it.polimi.dei.bosp;

import it.polimi.dei.bosp.dto.Result;

import java.util.Calendar;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public class STHormFDService extends BbqueService {

	/* *
	 * STHormFDService internal constants. Start from 15 because it extends
	 * BbqueService
	 */

	public static final int MSG_FILTER_ON = 15;
	public static final int MSG_DECODED = 16;
	public static final int MSG_SEND_BUFFER = 17;
	public static final int MSG_SET_SIZE = 17;

	private int frameWidth = 320;
	private int frameHeight = 240;
	private static int maxCoordinates = 1024;
	private static Paint paint = null;
	private static boolean filterEnabled;

	private final byte[] cameraBuffer = null;
	private final byte[][] doubleBuffer = new byte[2][];
	private int dbIndex = 0;
	private int currentRead = 0;

	private boolean firstRun = true;

	private long previoustimestamp = 0;
	private long currenttimestamp = 0;
	private final long timezero = (Calendar.getInstance().getTimeInMillis());

	@Override
	public IBinder onBind(Intent intent) {
		return cMessenger.getBinder();
	}

	/**
	 * onCreate() Initialise the Paint object, and the STHorm shared library
	 * */
	@Override
	public void onCreate() {
		super.onCreate();
		paint = new Paint();
		paint.setStyle(Paint.Style.STROKE);
		paint.setColor(Color.RED);
		FDetectInit(ImageType.ARGB_8888, frameWidth, frameHeight);
	}

	/**
	 * Instantiate the target - to be sent to clients - to communicate with this
	 * instance of STHormFDService.
	 */
	final Messenger cMessenger = new Messenger(new STHormMessageHandler());

	/**
	 * Handler of incoming messages from clients. This Handler overloads the
	 * BbqueMessageHandler.
	 */
	class STHormMessageHandler extends BbqueMessageHandler {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_FILTER_ON:
				filterEnabled = (msg.arg1 == 1 ? true : false);
				Log.d(TAG, "Filter Enabled: " + filterEnabled);
				break;
			case MSG_SEND_BUFFER:
				Log.d(TAG, "Buffer received from Activity...");
				// Save each frame alternatively to one or the other position
				// of the double buffer.
				doubleBuffer[dbIndex] = (byte[]) msg.obj;
				dbIndex = (dbIndex == 0 ? 1 : 0);
				frameHeight = msg.arg1;
				frameWidth = msg.arg2;
				break;
			case MSG_SET_CPS:
				Log.d(TAG, "New fps parameter received. Calling setCPS("
						+ (msg.obj).toString() + ")");
				setCPS(msg.replyTo, Float.parseFloat((msg.obj).toString()));
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	/**
	 * Converts from byte[] to Bitmap
	 **/
	private Bitmap byteArrayToBitmap(byte[] inputFrame) {
		byte[] currentFrame = inputFrame;
		int[] out = new int[(frameWidth * frameHeight)];
		decodeYUV420SP(out, currentFrame, frameWidth, frameHeight);
		return Bitmap.createBitmap(out, frameWidth, frameHeight,
				Config.ARGB_8888);
	}

	static private void decodeYUV420SP(int[] rgb, byte[] yuv420sp, int width,
			int height) {
		final int frameSize = width * height;

		for (int j = 0, yp = 0; j < height; j++) {
			int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
			for (int i = 0; i < width; i++, yp++) {
				int y = (0xff & (yuv420sp[yp])) - 16;
				if (y < 0)
					y = 0;
				if ((i & 1) == 0) {
					v = (0xff & yuv420sp[uvp++]) - 128;
					u = (0xff & yuv420sp[uvp++]) - 128;
				}
				int y1192 = 1192 * y;
				int r = (y1192 + 1634 * v);
				int g = (y1192 - 833 * v - 400 * u);
				int b = (y1192 + 2066 * u);

				if (r < 0)
					r = 0;
				else if (r > 262143)
					r = 262143;
				if (g < 0)
					g = 0;
				else if (g > 262143)
					g = 262143;
				if (b < 0)
					b = 0;
				else if (b > 262143)
					b = 262143;

				rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000)
						| ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
			}
		}
	}

	/* *
	 * Customized implementation of Barbeque callback methods
	 */

	/**
	 * onRun() Asks to the Activity for frames, when needed. This implementation
	 * is based on a double buffer, which is being initialized at the beginning
	 * depending on the value of the firstRun boolean. Then, for each current
	 * frame read, it asks the activity to send another one, which then replaces
	 * the frame not needed anymore, to be read during the next cycle. After
	 * each processing, it sends the frame back to the Activity through the
	 * Messenger as MSG_DECODED
	 */
	@Override
	public int onRun() {
		int cycles = EXCCycles();
		previoustimestamp = currenttimestamp;
		currenttimestamp = ((Calendar.getInstance().getTimeInMillis()) - timezero);
		long timestamp = currenttimestamp - previoustimestamp;
		Log.d(TAG, "onRun() called, cycle: " + cycles
				+ " time to the previous onRun: " + timestamp);
		// TODO: Move the following code somewhere else
		if (firstRun) {
			// Initialize buffer:
			while (doubleBuffer[0] == null) {
				// Ask for 2 frames, to initialize the buffer:
				Message askFrame = Message.obtain(null,
						STHormFDService.MSG_SEND_BUFFER, 0, 2);
				try {
					replyTo.send(askFrame);
					Log.d(TAG, "Asking frames to activity");
				} catch (RemoteException e) {
					Log.e(TAG, "Can't ask for frames to activity");
				}
			}
			firstRun = false;
		}
		if (doubleBuffer[currentRead] != null) {
			Log.d(TAG, "bitmap[" + currentRead + "] is not null!");
			Bitmap bitmap = null;
			int nbCoordinates = 0;
			// Get bitmap out of the original frame
//			BitmapFactory.Options options = new BitmapFactory.Options();
//			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//			bitmap = BitmapFactory.decodeByteArray(doubleBuffer[currentRead],
//					0, doubleBuffer[currentRead].length, options);
			bitmap = byteArrayToBitmap(doubleBuffer[currentRead]);
			Log.d(TAG, "onRun() -> bitmap is: "
					+ (bitmap == null ? "null" : "not null"));
			// Now we can release the frame, and ask for another one:
			// ******************************************************
			Message askFrame = Message.obtain(null,
					STHormFDService.MSG_SEND_BUFFER, 0, 1);
			try {
				replyTo.send(askFrame);
				Log.d(TAG, "Asking frames to activity");
			} catch (RemoteException e) {
				Log.e(TAG, "Can't ask for frames to activity");
			}
			// ******************************************************
			// And switch the current reading frame for the next iteration
			currentRead = (currentRead == 0 ? 1 : 0);
			if ((bitmap != null) && (filterEnabled)) {
				Log.d(TAG, "onRun() -> bitmap retrieved, filter enabled");

				int[] coordinates = null;
				int[] rgbaPix;

				frameWidth = bitmap.getWidth();
				frameHeight = bitmap.getHeight();
				rgbaPix = new int[frameWidth * frameHeight];

				bitmap.getPixels(rgbaPix, 0, frameWidth, 0, 0, frameWidth,
						frameHeight);

				// maxCoordinates = 1024;
				coordinates = new int[maxCoordinates * 4];
				Log.d(TAG, ">>>>>Entering  SThorm Realm");

				/** Native call */
				FDetectRun(rgbaPix, 1, 40, 0, coordinates, maxCoordinates);
				Log.d(TAG, "<<<<<Leaving   SThorm Realm");
				nbCoordinates = coordinates[0];

				Bitmap mutableBitmap = bitmap.copy(Bitmap.Config.ARGB_8888,
						true);
				Canvas c = new Canvas(mutableBitmap);
				int i;
				int ii = 4;
				for (i = 0; i < nbCoordinates; i++) {

					  Log.d(TAG, " x1:" + new Integer(coordinates[(4 * (i + 1))
					  + 0]).toString() + " y1:" + new Integer(coordinates[(4 *
					  (i + 1)) + 1]).toString() + " x2:" + new
					  Integer(coordinates[(4 * (i + 1)) + 2]).toString() +
					  " y2:" + new Integer(coordinates[(4 * (i + 1)) +
					  3]).toString());

					c.drawRect(cycles+coordinates[ii++], cycles+coordinates[ii++],
							cycles+coordinates[ii++], cycles+coordinates[ii++], paint);
				}
				bitmap = mutableBitmap;
			}

			// [@thoeni] Output image with a red rectangle indicating the
			// face
			Result result = new Result(bitmap, nbCoordinates);

			if (result.getBitmap() != null) {
				Log.d(TAG, "onRun() -> sending message frame to Activity...");
				Bundle frame = new Bundle();
				frame.putParcelable("bitmap", result.getBitmap());
				Message msg = Message.obtain(null, MSG_DECODED, result.getNb(),
						0);
				msg.setData(frame);
				try {
					replyTo.send(msg);
				} catch (Exception e) {

				}
			} else {
				Log.e(TAG,
						"onRun() -> Failed sending message frame to Activity!");
			}
		}
		return 0;
	}

	/**
	 * onRelease de-initialise the STHorm shared library
	 **/
	@Override
	public int onRelease() {
		Log.d(TAG, "onRelease called");
		intent.putExtra("BBQ_DEBUG", "onRelease called");
		intent.putExtra("ON_RELEASE", 1);
		sendBroadcast(intent);
		FDetectDeinit();
		return 0;
	}

	/**
	 * onConfigure To be implemented, it will handle all the runtime
	 * reconfiguration imposed by Bbque
	 **/
	@Override
	public int onConfigure(int awm_id) {
		// TODO Auto-generated method stub
		return super.onConfigure(awm_id);
	}

	// JNI Init ////////////////////////////////////////////////////////////
	public native int FDetectInit(ImageType imageType, int width, int height);

	public native static int FDetectRun(int[] image, int accuracy, int faceMin,
			int faceMax, int[] coordinates, int maxCoordinates);

	public native int FDetectDeinit();

	public enum ImageType {
		ALPHA_8(0), ARGB_8888(1), RGB_888(2);
		private final int imType;

		ImageType(int imType) {
			this.imType = imType;
		}
	}

	static {
		Log.d(">>>>> load NewSThormFDetect.so", "!");
		System.loadLibrary("NewSThormFDetect");
		Log.d("<<<<< load NewSThormFDetect.so", "!");
	}

	// /////////////////////////////////////////////////////////////////////////
}
