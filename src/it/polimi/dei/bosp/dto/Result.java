package it.polimi.dei.bosp.dto;

import android.graphics.Bitmap;

public class Result {

	Bitmap bitmap;
	int nb;

	public Result(Bitmap bitmap, int nb) {
		this.bitmap = bitmap;
		this.nb = nb;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public int getNb() {
		return nb;
	}
}
