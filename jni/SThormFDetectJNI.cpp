#include <jni.h>
#include <android/log.h>

#undef  WITH_STHORM
//#define WITH_STHORM 1

#ifdef __cplusplus
extern "C" {
#endif

#ifdef WITH_STHORM
#include <DetectMultiScale.h>
#endif

#define LOG_TAG "LIBSThormFDetect.SO"
#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define LOGF(...)  __android_log_print(ANDROID_LOG_FATAL,LOG_TAG,__VA_ARGS__)
//LOGD("test log in JNI, x is %d, y is %d\n",x,y);

// /// //// ///// ////// //////// ///////// ///////// /////////// ///////////

/*
 * Class:     st_sthorm_NewSTHormFDActivity
 * Method:    FDetectInit
 * Signature: (Lcom/SThorm/FDetect/NewSTHormFDActivity/ImageType;II)I
 */

JNIEXPORT jint JNICALL 
Java_it_polimi_dei_bosp_STHormFDService_FDetectInit (JNIEnv *, jobject, jobject, jint w, jint h) {
	LOGD("FDetectInit, WIDTH:%d, HEIGHT:%d\n", w, h);
#ifdef WITH_STHORM
	//int mf_DetectInit
	//           (detect_imageType_e imageType, int width, int height);
	mf_DetectInit(ARGB_8888,                    w,         h);
#endif
	return (0);
}

/*
 * Class:     st_sthorm_NewSTHormFDActivity
 * Method:    FDetectRun
 * Signature: ([IIII[II)I
 */
JNIEXPORT jint JNICALL Java_it_polimi_dei_bosp_STHormFDService_FDetectRun
//JNIEnv*,    jobject,      jintArray,       jint,          jint,         jint,         jintArray,              jint);
(JNIEnv* env, jobject thiz, jintArray image, jint accuracy, jint faceMin, jint faceMax, jintArray coordinates , jint maxCoordinates) {
	jint* _image        = env->GetIntArrayElements(image, 0);
	jint* _coordinates  = env->GetIntArrayElements(coordinates, 0);

	//jclass cls = env->GetObjectClass(thiz);//OK
	//jfieldID fid = env->GetFieldID(cls, "readyIdx" , "I");//OK

	int nbFaces;
	void* event;
	int* _shiftedCoordinates=_coordinates+4;
	void* dummy_userContext;

#ifdef WITH_STHORM
	//int   mf_DetectRun(void *userContext, char *image, int accuracy, int faceMin, int faceMax, DetectRectangle *coordinates, int maxCoordinates, int *nbFaces, void **event);
	int r = mf_DetectRun(dummy_userContext, (char *)_image, accuracy, faceMin, faceMax, (DetectRectangle*)_shiftedCoordinates, maxCoordinates, &nbFaces, &event);
	if (r == 1)
		LOGE("FDetectRun, Returns Error\n");

	//LOGD(">>> FDetectWait()\n");
	//int mf_DetectWait
	//           (void *event, void **userContext, DetectRectangle **coordinates, int **nbFaces);
	mf_DetectWait(event,       NULL,               NULL,                          NULL);
	//LOGD("<<< FDetectWait()\n");

	if (r == 0)
		LOGD("FDetectRun, nbFaces:%d\n", nbFaces);

#endif

	_coordinates[0]=nbFaces;


	//Test
	nbFaces=1;
	_coordinates[ 0]=nbFaces;
	_coordinates[ 4]=1;_coordinates[ 5]=1;_coordinates[ 6]=10;_coordinates[ 7]=20;


	env->ReleaseIntArrayElements(coordinates, _coordinates, 0);
	env->ReleaseIntArrayElements(image, _image, 0);
	return (0);
}

/*
 * Class:     st_sthorm_NewSTHormFDActivity
 * Method:    FDetectDeinit
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_it_polimi_dei_bosp_STHormFDService_FDetectDeinit
(JNIEnv *, jobject) {
	LOGD("FDetectDeinit\n");
#ifdef WITH_STHORM
	//int mf_DetectDeinit();
	mf_DetectDeinit();
#endif
	return (0);
}

/*
 * Class:     st_sthorm_NewSTHormFDActivity
 * Method:    FDetectDeinit
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_it_polimi_dei_bosp_STHormFDService_pippo
(JNIEnv *, jobject) {
	LOGD("FDetectDeinit\n");
	return (0);
}

#ifdef __cplusplus
}
#endif
