LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE     := NewSThormFDetect
LOCAL_C_INCLUDES := $(P2012_KERNELS_PATH)/include/
LOCAL_C_INCLUDES += $(P2012_PKG_DIR)/modules/OpenCL/ClamPkg/include/
LOCAL_SRC_FILES  := SThormFDetectJNI.cpp

LOCAL_LDLIBS     := -L$(SYSROOT)/usr/lib -llog -ljnigraphics
#LOCAL_LDLIBS     += -L$(P2012_KERNELS_PATH)/host/android/
#LOCAL_LDLIBS     += -lp12DetectMultiScale_OCL 

include $(BUILD_SHARED_LIBRARY)
